/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "TrigMultiVarHypo/TrigL2CaloRingerFex.h"
#include "TrigMultiVarHypo/TrigL2CaloRingerHypo.h"
#include "../TrigL2CaloRingerFexMT.h"


DECLARE_COMPONENT( TrigL2CaloRingerFex )
DECLARE_COMPONENT( TrigL2CaloRingerHypo )
DECLARE_COMPONENT( TrigL2CaloRingerFexMT )


